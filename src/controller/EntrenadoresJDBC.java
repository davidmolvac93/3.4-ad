/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelos.Entrenadores;
import modelos.EntrenadoresDAO;
import modelos.Jugadores;

/**
 *
 * @author david
 */
public class EntrenadoresJDBC implements EntrenadoresDAO {

    Connection conn;

    //con paginación
    private static final String SELECT_ALL_ENTRENADORES_PAGINATION = "select * from entrenadores order by idEntrenador limit ? offset ?";
    private static final String SELECT_ALL_ENTRENADORES = "select * from entrenadores";
    private static final String SELECT_ENTRENADORES = "select * from entrenadores where idEntrenador = ?";
    private static final String SELECT_ENTRENADORES_BY_NOMBRE = "select * from entrenadores where nombre LIKE ? order by idEntrenador limit ? offset ?";
    private static final String SELECT_ENTRENADORES_BY_TITULOS = "select * from entrenadores where titulos = ? order by idEntrenador limit ? offset ?";
    private static final String INSERT_ENTRENADORES_QUERY = "insert into entrenadores (nombre, edad, cargo, salario, titulos )values";
    private static final String DELETE_ENTRENADORES = "delete from entrenadores where idEntrenador = ?";
    private static final String UPDATE_ENTRENADORES = "update entrenadores SET nombre = ?, edad= ?, cargo = ? , salario = ? , titulos = ?  where idEntrenador = ?";
    private static final String SELECT_LAST_ID = "select MAX(idEntrenador) as idEntrenador from entrenadores";

    public EntrenadoresJDBC(Connection conn) {

        super();
        this.conn = conn;

    }

    @Override

    public List<Entrenadores> listaEntreandores(int desde, int cuantos) {

        PreparedStatement stmt;
        List<Entrenadores> entrenador = new ArrayList<Entrenadores>();

        try {

            stmt = conn.prepareStatement(SELECT_ALL_ENTRENADORES_PAGINATION);
            stmt.setInt(1, cuantos);
            stmt.setInt(2, desde);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                int idEntrenador = rs.getInt("idEntrenador");
                String nombreEntrenador = rs.getString("nombre");
                int edadEntrenador = rs.getInt("edad");
                String cargoEntrenador = rs.getString("cargo");
                int salarioEntrenador = rs.getInt("salario");
                int titulosEntrenador = rs.getInt("titulos");

                Entrenadores entrenadores = new Entrenadores(nombreEntrenador, edadEntrenador, cargoEntrenador, salarioEntrenador, titulosEntrenador);
                entrenadores.setIdEntrenador(idEntrenador);
                entrenador.add(entrenadores);

            }
        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");
        }

        System.out.println(entrenador.size());
        return entrenador;

    }

    @Override
    public boolean insertEntrenadores(String nombre, int edad, String cargo, int salario, int titulos) {

        Statement stmt = null;
        Statement stmt2 = null;
        Entrenadores entrenador = null;

        int numero;
        boolean comprobar = false;

        try {

            stmt2 = conn.createStatement();
            ResultSet rs2 = stmt2.executeQuery(SELECT_LAST_ID);
            rs2.last();

            int idEntrenador = rs2.getInt("idEntrenador");
            idEntrenador += 1;
            stmt = conn.createStatement();

            //EJECUCION DE LA INSERCION
            numero = stmt.executeUpdate(INSERT_ENTRENADORES_QUERY + "( \"" + nombre + " \", " + edad + " ,\" " + cargo + " \", " + salario + " , " + titulos + " )");

            if (numero == 1) {
                comprobar = true;
                entrenador = new Entrenadores(nombre, edad, cargo, salario, titulos);
                System.out.println("Hemos insertado el nuevo entrenador");

            } else {

                System.err.print("ERROR, no hemos podido insertar la fila");
                entrenador = null;
                comprobar = false;
            }
        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");

        } finally {

            try {
                //Liberamos todos los recursos pase lo que pase
                if (stmt != null) {
                    stmt.close();
                }
                if (stmt2 != null) {
                    stmt2.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }

        return comprobar;
    }

    @Override
    public boolean updateEntrenadores(Entrenadores entrenador) {

        boolean salida = false;
        Entrenadores entrenadorUpdate = getEntrenadores(entrenador.getIdEntrenador());

        if (entrenadorUpdate != null) {

            if (entrenador.getNombre().equals(entrenadorUpdate.getNombre()) && entrenador.getEdad() == entrenadorUpdate.getEdad() && entrenador.getCargo().equals(entrenadorUpdate.getCargo()) && entrenador.getSalario() == entrenadorUpdate.getSalario() && entrenador.getTitulos() == entrenadorUpdate.getTitulos()) {

                System.out.println("Los valores introducidos son los mismo a los anteriores");

            } else {

                PreparedStatement preparedStatement;

                try {

                    preparedStatement = conn.prepareStatement(UPDATE_ENTRENADORES);
                    preparedStatement.setString(1, entrenador.getNombre());
                    preparedStatement.setInt(2, entrenador.getEdad());
                    preparedStatement.setString(3, entrenador.getCargo());
                    preparedStatement.setDouble(4, entrenador.getSalario());
                    preparedStatement.setInt(5, entrenador.getTitulos());
                    preparedStatement.setInt(6, entrenador.getIdEntrenador());

                    int numero = preparedStatement.executeUpdate();

                    if (numero == 1) {

                        entrenadorUpdate = entrenador;
                        salida = true;
                        System.out.println("Hemos actualizado el entrenador");

                    } else {

                        entrenadorUpdate = entrenador;
                        salida = false;
                        System.err.print("No hemos podido actualizar al entrenador");

                    }

                } catch (SQLException excpt) {

                    excpt.printStackTrace();
                    System.err.print("ERROR");

                }
            }

        } else {

            System.out.println("Error, no existe un entrenador con ese ID");
        }
        return salida;
    }

    @Override
    public boolean deleteEntrenadores(int idEntrenador) {

        Entrenadores entrenador = getEntrenadores(idEntrenador);
        boolean salida = false;

        if (entrenador != null) {

            PreparedStatement stmt = null;

            try {

                stmt = conn.prepareStatement(DELETE_ENTRENADORES);
                stmt.setInt(1, idEntrenador);
                int numero = stmt.executeUpdate();

                if (numero == 1) {
                    //SALIO TODO BIEN
                    salida = true;
                    System.out.println("Hemos borrado al entrenador con ID " + idEntrenador + " al carreeeeeeeeer");
                } else {

                    salida = false;

                }
            } catch (SQLException excpt) {

                excpt.printStackTrace();
            } finally {

                try {

                    if (stmt != null) {
                        stmt.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            System.err.print("ERROR, no hemos encontrado un entrenador con ese id " + idEntrenador);
            salida = false;
        }
        return salida;
    }

    @Override
    public Entrenadores getEntrenadores(int idEntrenador) {

        PreparedStatement stmt;
        Entrenadores entrenador = null;

        try {

            stmt = conn.prepareStatement(SELECT_ENTRENADORES);
            stmt.setInt(1, idEntrenador);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String nombreEntrenador = rs.getString("nombre");
                int edadEntrenador = rs.getInt("edad");
                String cargoEntrenador = rs.getString("cargo");
                double salarioEntrenador = rs.getDouble("salario");
                int titulosEntrenador = rs.getInt("titulos");

                 entrenador = new Entrenadores(nombreEntrenador, edadEntrenador, cargoEntrenador, salarioEntrenador, titulosEntrenador);
                 entrenador.getIdEntrenador();
                 entrenador.setIdEntrenador(idEntrenador);
                 
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");
        }
        
        System.out.println(entrenador);
        return entrenador;
    }

    @Override
    public List<Entrenadores> getEntrenadorLike(String nombre, int desde, int cuantos) {

        List<Entrenadores> salida = new ArrayList<Entrenadores>();
        PreparedStatement preparedStatement;

        try {

            preparedStatement = conn.prepareStatement(SELECT_ENTRENADORES_BY_NOMBRE);
            preparedStatement.setString(1, "%" + nombre + "%");
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int idEntrenador = rs.getInt("idEntrenador");
                String nombreEntrenador = rs.getString("nombre");
                int edadEntrenador = rs.getInt("edad");
                String cargoEntrenador = rs.getString("cargo");
                double salarioEntrenador = rs.getDouble("salario");
                int titulosEntrenador = rs.getInt("titulos");

                Entrenadores entrenador = new Entrenadores(nombreEntrenador, edadEntrenador, cargoEntrenador, salarioEntrenador, titulosEntrenador);
                entrenador.setIdEntrenador(idEntrenador);
                salida.add(entrenador);

            }

            if (salida.isEmpty()) {

                salida = null;

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");
        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public List<Entrenadores> getEntrenadorByTitulos(int titulos, int desde, int cuantos) {

        List<Entrenadores> salida = new ArrayList<Entrenadores>();
        PreparedStatement preparedStatement;

        try {

            preparedStatement = conn.prepareStatement(SELECT_ENTRENADORES_BY_TITULOS);
            preparedStatement.setInt(1, titulos);
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {

                int idEntrenador = rs.getInt("idEntrenador");
                String nombreEntrenador = rs.getString("nombre");
                int edadEntrenador = rs.getInt("edad");
                String cargoEntrenador = rs.getString("cargo");
                double salarioEntrenador = rs.getDouble("salario");
                int titulosEntrenador = rs.getInt("titulos");

                Entrenadores entrenador = new Entrenadores(nombreEntrenador, edadEntrenador, cargoEntrenador, salarioEntrenador, titulosEntrenador);
                entrenador.setIdEntrenador(idEntrenador);
                salida.add(entrenador);
            }

            if (salida.isEmpty()) {

                salida = null;
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");

        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public int allEntrenadores() {

        Statement stmt;
        int conteo = 0;

        try {

            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_ENTRENADORES);

            while (rs.next()) {
                conteo++;
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");
        }

        System.out.println(conteo);
        return conteo;
    }

    @Override
    public int lastID() {

        Statement stmt;

        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_LAST_ID);

            while (rs.next()) {
                System.out.println("El ultimo ID es: " + rs.getString(1));
            }
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return 0;
        
    }

}
