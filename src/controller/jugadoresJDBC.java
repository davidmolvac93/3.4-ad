/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.Conexion;
import modelos.Jugadores;
import modelos.JugadoresDAO;

/**
 *
 * @author david
 */
public class jugadoresJDBC implements JugadoresDAO {

    Connection conn;

    //con paginación
    private static final String INSERT_JUGADORES_QUERY = "insert into jugadores (nombre,edad,posicion , goles, salarioSemanal, lesionado, dorsal)values";
    private static final String SELECT_ALL_JUGADORES_PAGINATION = "select * from jugadores order by idJugador limit ? offset ?";
    private static final String SELECT_JUGADOR = "select * from jugadores where idJugador = ?";
    private static final String DELETE_JUGADOR = "delete from jugadores where idJugador = ?";
    private static final String SELECT_ALL_JUGADORES = "select * from jugadores";
    private static final String SELECT_JUGADOR_BY_NOMBRE = "select * from jugadores where nombre LIKE ? order by idJugador limit ? offset ?";
    private static final String SELECT_JUGADOR_BY_GOLES = "select * from jugadores where goles = ? order by idJugador limit ? offset ?";
    private static final String UPDATE_JUGADOR = "update jugadores SET nombre = ?, edad= ?, posicion = ? , goles = ? , salarioSemanal = ? , lesionado = ? , dorsal = ? where idJugador = ?";
    private static final String SELECT_LAST_ID = "select MAX(idJugador) as idJugador from jugadores";

    public jugadoresJDBC(Connection conn) {

        super();
        this.conn = conn;

    }

    public jugadoresJDBC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Jugadores> listaJugadores(int desde, int cuantos) {

        PreparedStatement stmt;
        List<Jugadores> jugadores = new ArrayList<Jugadores>();

        try {

            stmt = conn.prepareStatement(SELECT_ALL_JUGADORES_PAGINATION);
            stmt.setInt(1, cuantos);
            stmt.setInt(2, desde);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int idJugador = rs.getInt("idJugador");
                String nombreJugador = rs.getString("nombre");
                int edadJugador = rs.getInt("edad");
                String posicionJugador = rs.getString("posicion");
                int golesJugador = rs.getInt("goles");
                double salarioSemanalJugador = rs.getDouble("salarioSemanal");
                boolean lesionadoJugador = rs.getBoolean("lesionado");
                int dorsalJugador = rs.getInt("dorsal");

                Jugadores jugador = new Jugadores(nombreJugador, edadJugador, posicionJugador, golesJugador, salarioSemanalJugador, lesionadoJugador, dorsalJugador);
                jugador.setIdJugador(idJugador);
                jugadores.add(jugador);
            }

            System.out.println(jugadores.size());
            
        } catch (SQLException excpt) {

            excpt.printStackTrace();
            jugadores = null;
        }
        return jugadores;
    }

    @Override
    public Jugadores getJugador(int idJugador) {

        PreparedStatement stmt;
        Jugadores jugador = null;
        boolean salida;

        try {

            stmt = conn.prepareStatement(SELECT_JUGADOR);
            stmt.setInt(1, idJugador);   //DUDA DE QUE PASAR COMO PRIMER PARAMETRO
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {

                String nombreJugador = rs.getString("nombre");
                int edadJugador = rs.getInt("edad");
                String posicionJugador = rs.getString("posicion");
                int golesJugador = rs.getInt("goles");
                double salarioSemanalJugador = rs.getDouble("salarioSemanal");
                boolean lesionadoJugador = rs.getBoolean("lesionado");
                int dorsalJugador = rs.getInt("dorsal");

                jugador = new Jugadores(nombreJugador, edadJugador, posicionJugador, golesJugador, salarioSemanalJugador, lesionadoJugador, dorsalJugador);
                jugador.setIdJugador(idJugador);

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
        }

        System.out.println(jugador);
        return jugador;
    }

    @Override
    public boolean insertJugador(String nombre, int edad, String posicion, int goles, double salarioSemanal, boolean lesionado, int dorsal) {

        Statement stmt = null;
        Statement stmt2 = null;
        Jugadores jugador = null;

        int numero;
        boolean comprobar = false;

        try {

            stmt2 = conn.createStatement();         //ERROR A LA HORA DE EJECUTAR EL INSERT
            ResultSet rs2 = stmt2.executeQuery(SELECT_LAST_ID);
            rs2.last();

            int idJugador = rs2.getInt("idJugador");    //DUDA DE SI AL AUTOINCREMENTAR LO DEBEMOS METER
            idJugador += 1;
            stmt = conn.createStatement();

            //EJECUCION DE LA INSERCION
            numero = stmt.executeUpdate(INSERT_JUGADORES_QUERY + "( \"" + nombre + " \", " + edad + " ,\" " + posicion + " \", " + goles + " , " + salarioSemanal + " , " + lesionado + " , " + dorsal + ")");

            if (numero == 1) {
                comprobar = true;
                jugador = new Jugadores(nombre, edad, posicion, goles, salarioSemanal, lesionado, dorsal);
                System.out.println(jugador);

            } else {

                System.err.print("ERROR, no hemos podido insertar la fila");
                jugador = null;
                comprobar = false;
            }

        } catch (SQLException excpt) {

            System.err.print(excpt.getMessage());

        } finally {
            try {
                //Liberamos todos los recursos pase lo que pase
                if (stmt != null) {
                    stmt.close();
                }
                if (stmt2 != null) {
                    stmt2.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }
        
        return comprobar;
    }

    @Override
    public boolean updateJugador(Jugadores jugador) {

        boolean salida = false;
        Jugadores jugadorUpdate = getJugador(jugador.getIdJugador());

        if (jugadorUpdate != null) {

            if (jugador.getNombre().equals(jugadorUpdate.getNombre()) && jugador.getEdad() == jugadorUpdate.getEdad() && jugador.getPosicion().equals(jugadorUpdate.getPosicion()) && jugador.getGoles() == jugadorUpdate.getGoles() && jugador.getSalarioSemanal() == jugadorUpdate.getSalarioSemanal() && jugador.isLesionado() == jugadorUpdate.isLesionado() && jugador.getDorsal() == jugadorUpdate.getDorsal()) {

                System.out.println("Los valores son exactamente los mismos a los introducidos");

            } else {

                PreparedStatement preparedStatement;

                try {

                    preparedStatement = conn.prepareStatement(UPDATE_JUGADOR);
                    preparedStatement.setString(1, jugador.getNombre());
                    preparedStatement.setInt(2, jugador.getEdad());
                    preparedStatement.setString(3, jugador.getPosicion());
                    preparedStatement.setInt(4, jugador.getGoles());
                    preparedStatement.setDouble(5, jugador.getSalarioSemanal());
                    preparedStatement.setBoolean(6, jugador.isLesionado());
                    preparedStatement.setInt(7, jugador.getDorsal());
                    preparedStatement.setInt(8, jugador.getIdJugador());

                    int numero = preparedStatement.executeUpdate();

                    if (numero == 1) {

                        jugadorUpdate = jugador;
                        salida = true;
                        System.out.println("Hemos actualizado al jugador");

                    } else {

                        jugadorUpdate = null;
                        salida = false;
                        System.out.println("No podemos actualizar al jugador");
                    }

                } catch (SQLException excpt) {

                    excpt.printStackTrace();
                    System.err.print("ERROR");
                }
            }

        } else {

            System.out.println("Error: no existe un jugador con ese id.");

        }
        return salida;
    }

    @Override
    public boolean deleteJugador(int idJugador) {

        Jugadores jugador = getJugador(idJugador);
        boolean salida = false;

        if (jugador != null) {

            PreparedStatement stmt = null;

            try {

                stmt = conn.prepareStatement(DELETE_JUGADOR);
                stmt.setInt(1, idJugador);      //MISMA DUDA CON EL PARAMETRO
                int numero = stmt.executeUpdate();

                if (numero == 1) {
                    //SALIO TODO BIEN
                    salida = true;
                    System.out.println("Hemos borrado al jugador con ID " + idJugador + " al carreeeeeeeeer");
                } else {

                    salida = false;
                }
            } catch (SQLException excpt) {

                excpt.printStackTrace();

            } finally {

                try {

                    if (stmt != null) {
                        stmt.close();
                    }

                } catch (SQLException excpt) {

                    excpt.printStackTrace();
                }
            }

        } else {
            System.err.print("ERROR, no hemos encontrado un jugador con ese " + idJugador);
            salida = false;
        }

        return salida;
    }

    @Override
    //PARA BUSCAR JUGADOR POR SU NOMBRE
    public List<Jugadores> getJugadorLike(String nombre, int desde, int cuantos) {

        List<Jugadores> salida = new ArrayList<Jugadores>();
        PreparedStatement preparedStatement;

        try {

            preparedStatement = conn.prepareStatement(SELECT_JUGADOR_BY_NOMBRE);
            preparedStatement.setString(1, "%" + nombre + "%");
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int idJugador = rs.getInt("idJugador");
                String nombreJugador = rs.getString("nombre");
                int edadJugador = rs.getInt("edad");
                String posicionJugador = rs.getString("posicion");
                int golesJugador = rs.getInt("goles");
                double salarioSemanalJugador = rs.getDouble("salarioSemanal");
                boolean lesionadoJugador = rs.getBoolean("lesionado");
                int dorsalJugador = rs.getInt("dorsal");

                Jugadores jugador = new Jugadores(nombreJugador, edadJugador, posicionJugador, golesJugador, salarioSemanalJugador, lesionadoJugador, dorsalJugador);
                jugador.setIdJugador(idJugador);
                salida.add(jugador);
                System.out.println(jugador);

            }
            

            if (salida.isEmpty()) {

                salida = null;
            }

        } catch (SQLException excpt) {
            excpt.printStackTrace();
            System.err.print("ERROR");

        }
        System.out.println(salida);
        return salida;
    }

    @Override

    public List<Jugadores> getJugadorByGoles(int goles, int desde, int cuantos) {

        List<Jugadores> salida = new ArrayList<Jugadores>();
        PreparedStatement preparedStatement;

        try {

            preparedStatement = conn.prepareStatement(SELECT_JUGADOR_BY_GOLES);
            preparedStatement.setInt(1, goles);
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int idJugador = rs.getInt("idJugador");
                String nombreJugador = rs.getString("nombre");
                int edadJugador = rs.getInt("edad");
                String posicionJugador = rs.getString("posicion");
                int golesJugador = rs.getInt("goles");
                double salarioSemanalJugador = rs.getDouble("salarioSemanal");
                boolean lesionadoJugador = rs.getBoolean("lesionado");
                int dorsalJugador = rs.getInt("dorsal");

                Jugadores jugador = new Jugadores(nombreJugador, edadJugador, posicionJugador, golesJugador, salarioSemanalJugador, lesionadoJugador, dorsalJugador);
                jugador.setIdJugador(idJugador);
                salida.add(jugador);

            }
            System.out.println(salida.size());

            if (salida.isEmpty()) {

                salida = null;

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print("ERROR");
        }

        return salida;
    }

    @Override

    public int allJugadores() {

        Statement stmt;
        int conteo = 0;

        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_JUGADORES);

            while (rs.next()) {
                conteo++;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.print("ERROR");

        }
        System.out.println(conteo);
        return conteo;
    }

    @Override
    public int lastID() {

        Statement stmt;

        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_LAST_ID);

            while (rs.next()) {
                System.out.println("El ultimo ID es: " + rs.getString(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return 0;

    }
}
