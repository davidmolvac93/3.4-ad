/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controlador.DirectivaControlador;
import controller.DirectivaJDBC;
import modelos.Directiva;

/**
 *
 * @author david
 */
public class TestBDDirectiva {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {
        
                
        int contador = 0;
        int a = 2;
        int b = 10;
        Class.forName("com.mysql.jdbc.Driver");
        DirectivaJDBC directivo = new DirectivaJDBC(DirectivaControlador.conectar());
        
        
//INSERTAR DIRECTIVO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 03");
        System.out.println("Query que se ejecuta: " + "insert into directiva (nombre, edad, salario , puesto)values"); //iNTRODUCIMOS LOS VALORES      
        System.out.println("Query que se ejecuta: SELECT_LAST_ID"); //
        System.out.println("Salida esperada: " + directivo.insertDirectivos("Guti Maricon", 40, 1000, "Asesor"));
        System.out.println("Salida obtenida: true");
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");       
        */
        
        
        //BORRAR DIRECTIVO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 04");
        System.out.println("Query que se ejecuta: delete from directiva where idDirectiva = 1001"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  DELETE_DIRECTIVA");  
        System.out.println("Salida esperada: true" );
        System.out.println("Salida obtenida: true");
        if(directivo.deleteDirectivos(1001)){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
       
        
        
        //SELECCIONAR TODOS LOS DIRECTIVO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 05");
        System.out.println("Query que se ejecuta: " + "select * from directiva"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  SELECT_ALL_DIRECTIVA");
        System.out.println("Salida esperada: " + directivo.allDirectivos());
        System.out.println("Salida obtenida: " + directivo.allDirectivos());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
      */
        
        //LISTAR LOS DIRECTIVO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 01");
        System.out.println("Query que se ejecuta: " + "select * from directiva order by idDirectiva limit 0 offset 10"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta: + SELECT_ALL_DIRECTIVA_PAGINATION");
        System.out.println("Salida esperada: " + 10);
        System.out.println("Salida obtenida: " + b);
        System.out.println(directivo.listaDirectivos(a, b));
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        
        //OBTENER DIRECTIVO POR SU NOMBRE
     
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 06");
        System.out.println("Query que se ejecuta: select * from directiva where nombre LIKE ? order by idDirectiva limit ? offset ?"); //Al fisio que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_DIRECTIVA_BY_NOMBRE");
        System.out.println("Salida esperada: " + directivo.getDirectivoLike("En", 25, 0).size());
        System.out.println("Salida obtenida: " + directivo.getDirectivoLike("En", 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");         
        */
        
        
        //OBTENER DIRECTIVO
        
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 02");
        System.out.println("Query que se ejecuta: " + "select * from directiva where idDirectiva = 2"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_DIRECTIVA");  
        System.out.println("Salida esperada: " + a);
        System.out.println("Salida obtenida: " + directivo.getDirectivos(a).getIdDirectiva());
        
        if(directivo.getDirectivos(a).getIdDirectiva()==a){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");

         */
        
        
        //ACTUALIZAR DIRECTIVO
       /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 08");
        System.out.println("Query que se ejecuta: update directiva SET nombre = ?, edad= ?, salario = ? , puesto = ?  where idDirectiva = ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  UPDATE_DIRECTIVA"); 
        Directiva directivo1 = new Directiva("Florentino Perez",40,52000,"Presidente");
        directivo1.setIdDirectiva(1);                                     //FUNCIONA
        System.out.println(directivo.updateDirectivos(directivo1)); 
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        //OBTENER DIRECTIVO POR EDAD
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 07");
        System.out.println("Query que se ejecuta: select * from directiva where edad = ? order by idDirectiva limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_DIRECTIVA_BY_EDAD");
        System.out.println("Salida esperada: " + directivo.getDirectivaByEdad(57, 25, 0));
        System.out.println("Salida obtenida: " + directivo.getDirectivaByEdad(57, 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");    
        */
        
        
        //OBTENER DIRECTIVO POR ULTIMO ID
  
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 09");
        System.out.println("Query que se ejecuta: select MAX(idDirectiva) as idDirectiva from directiva"); //Al entreandor que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_LAST_ID"); 
        System.out.println("Salida esperada " + directivo.lastID());
        System.out.println("Salida obtenida " + directivo.lastID());
        System.out.println("Resultado correcto ");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");        
        */
    }
    
}
