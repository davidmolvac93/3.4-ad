/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controlador.jugadorControlador;
import controller.jugadoresJDBC;
import java.util.List;
import modelos.Jugadores;

/**
 *
 * @author david
 */
public class TestBDJugador {

    public static void main(String[] args) throws ClassNotFoundException {

        int contador = 0;
        int a = 3;
        int b = 10;
        Class.forName("com.mysql.jdbc.Driver");
        jugadoresJDBC jugador = new jugadoresJDBC(jugadorControlador.conectar());

        
        //INSERTAR JUGADOR
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 03");
        System.out.println("Query que se ejecuta: " + "insert into jugadores (nombre,edad,posicion , goles, salarioSemanal, lesionado, dorsal)values"); //iNTRODUCIMOS LOS VALORES      
        System.out.println("Query que se ejecuta: SELECT_LAST_ID"); //
        System.out.println("Salida esperada: " + jugador.insertJugador("Haland", 20, "delantero", 40, 0, false, 15000));
        System.out.println("Salida obtenida: true");
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");       
        */
        
        
        //BORRAR JUGADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 04");
        System.out.println("Query que se ejecuta: delete from jugadores where idJugador = 1012"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  DELETE_JUGADOR");  
        System.out.println("Salida esperada: true" );
        System.out.println("Salida obtenida: true");
        if(jugador.deleteJugador(1012)){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");

        
        */
        
        
        //SELECCIONAR TODOS LOS JUGADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 05");
        System.out.println("Query que se ejecuta: " + "select * from jugadores"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  SELECT_ALL_JUGADORES");
        System.out.println("Salida esperada:  jugador.allJugadores()");
        System.out.println("Salida obtenida: " + jugador.allJugadores());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        //LISTAR LOS JUGADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 01");
        System.out.println("Query que se ejecuta: " + "select * from jugadores order by idJugador limit 0 offset 10"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta: + SELECT_ALL_JUGADORES_PAGINATION");
        System.out.println("Salida esperada: " + 10);
        System.out.println("Salida obtenida: " + b);
        System.out.println(jugador.listaJugadores(a, b));
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
         */
        
        
        //OBTENER JUGADOR POR SU NOMBRE
     
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 06");
        System.out.println("Query que se ejecuta: select * from jugadores where nombre LIKE ? order by idJugador limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_JUGADOR_BY_NOMBRE");
        System.out.println("Salida esperada: " + jugador.getJugadorLike("ra", 25, 0).size());
        System.out.println("Salida obtenida: " + jugador.getJugadorLike("ra", 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");         
        */
        
        
        //OBTENER JUGADOR
        /*
        
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 02");
        System.out.println("Query que se ejecuta: " + "select * from jugadores where idJugador = 3"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_JUGADOR");  
        System.out.println("Salida esperada: " + a);
        System.out.println("Salida obtenida: " + jugador.getJugador(a).getIdJugador());
        
        if(jugador.getJugador(a).getIdJugador()==a){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");

         */
        
        
        //ACTUALIZAR JUGADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 08");
        System.out.println("Query que se ejecuta: update jugadores SET nombre = ?, edad= ?, posicion = ? , goles = ? , salarioSemanal = ? , lesionado = ? , dorsal = ? where idJugador = ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  UPDATE_JUGADOR"); 
        Jugadores jugador1 = new Jugadores("Thibaut Courtois", 28, "Portero", 0, 10, false, 100);
        jugador1.setIdJugador(1);                                     //FUNCIONA
        System.out.println(jugador.updateJugador(jugador1)); 
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        //OBTENER JUGADORES POR GOLES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 07");
        System.out.println("Query que se ejecuta: select * from jugadores where goles = ? order by idJugador limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_JUGADOR_BY_GOLES");
        System.out.println("Salida esperada: " + jugador.getJugadorByGoles(4, 25, 0).size());
        System.out.println("Salida obtenida: " + jugador.getJugadorByGoles(4, 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");    
        */
        
        
        //OBTENER JUGADOR POR ULTIMO ID
  
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + "Controller Modelo 09");
        System.out.println("Query que se ejecuta: select MAX(idJugador) as idJugador from jugadores"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_LAST_ID"); 
        System.out.println("Salida esperada " + jugador.lastID());
        System.out.println("Salida obtenida " + jugador.lastID());
        System.out.println("Resultado correcto ");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");        
        */
    }

}
