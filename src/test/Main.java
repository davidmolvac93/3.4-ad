/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controlador.DirectivaControlador;
import controller.EntrenadoresJDBC;
import controller.jugadoresJDBC;
import controller.FisioterapeutaJDBC;
import controlador.entrenadoresControlador;
import controlador.jugadorControlador;
import controlador.FisioterapeutaControlador;
import controller.DirectivaJDBC;
import java.util.Scanner;
import modelos.Directiva;
import modelos.Entrenadores;
import modelos.Fisioterapeutas;

/**
 *
 * @author david
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        //EntrenadoresJDBC entrenador = new EntrenadoresJDBC(entrenadoresControlador.conectar());
        //jugadoresJDBC jugador = new jugadoresJDBC(jugadorControlador.conectar());
        FisioterapeutaJDBC fisio = new FisioterapeutaJDBC(FisioterapeutaControlador.conectar());       
        System.out.println(fisio.insertFisio("Pablo", 20, "Perez", 330));     //ERROR LINEA 97 NO SE COMO

        //DirectivaJDBC directivo = new DirectivaJDBC(DirectivaControlador.conectar());

        //boolean comprobacion = false;
        //mostrarTest("SELECT_JUGADOR","select * from jugadores where idJugador = 1",1,1,comprobacion);
        /*
        COMPROBAMOS DIRECTIVA: 
        
        System.out.println(directivo.insertDirectivos("Pablo", 30, 2000, "Director"));   //LOMISMO QUE EN LAS ANTERIORES
        System.out.println(directivo.getDirectivos(2));       //OBTENERPORID    ------  FUNCIONA
        System.out.println(directivo.allDirectivos());      //OBTENER TOTAL DIRECTIVOS    ----   FUNCIONA
        System.out.println(directivo.getDirectivaByEdad(57, 10, 0));      //OBTENERDIRECTIVOPOREDAD   ---- FUNCIONA
        System.out.println(directivo.getDirectivoLike("Florentino", 10, 0));     //OBTENERPORNOMBRE   ------  FUNCIONA
        System.out.println(directivo.lastID());           //OBTENERULTIMO ID -------    FUNCIONA
        System.out.println(directivo.listaDirectivos(0, 1));      //LISTA DE DIRECTIVOS      ---    FUNCIONA

        Directiva directivos = new Directiva("Florentino Perez",77,28.000,"Presidente");
        directivos.setIdDirectiva(1);                                           //FUNCIONA
        System.out.println(directivo.updateDirectivos(directivos));
         */
 /*
        COMPROBAMOS FISIOTERAPEUTAS:
      
        System.out.println(fisio.insertFisio("Pablo", 20, "Perez", 330));     ERROR LINEA 97 NO SE COMO
        System.out.println(fisio.deleteFisio(4));        DELETE     ---------     FUNCIONA
        System.out.println(fisio.getFisio(2));       GETFISIO    --------   FUNCIONA
        System.out.println(fisio.listaFisios(2, 0));        LISTARFISIO    ----  FUNCIONA
        System.out.println(fisio.getFisoterapeutasLike("Javier Mallo", 61, 0));       OBTENER NOMBRE   --  FUNCIONA
        System.out.println(fisio.getFisoterapeutasByEdad(40, 10, 0));      OBTENER EDAD FISIOS   ---- FUNCIONA          
        System.out.println(fisio.allFisoterapeutas());          OBTENER EL TOTAL DE FISIOS    ----     FUNCIONA
        System.out.println(fisio.lastID());      OBTENER ULTIMO ID   --    FUNCIONA
        
        
        Fisioterapeutas dd = new Fisioterapeutas("Javier Mallo", 45, "Preparador fisico", 7.2);
        dd.setIdFisioterapueta(3);                          //  FUNCIONA
        System.out.println(fisio.updateFisio(dd));
         */
 /*
        COMPROBAMOS ENTRENADORES :
        
        System.out.println(entrenador.insertEntrenadores("Roseti", 70, "Visionario", 600, 1));   //INSERT  -- FUNCIONA
        System.out.println(entrenador.deleteEntrenadores(5));    //DELETE   --   FUNCIONA
        System.out.println(entrenador.getEntrenadores(3));        //GETENTRENADOR   --    FUNCIONA
        System.out.println(entrenador.getEntrenadorLike("Zidane", 1, 0));           //GETNOMBRE    ---   FUNCIONA   
        System.out.println(entrenador.listaEntreandores(0, 2));      //LISTARENTRENADOR   --    FUNCIONA
        System.out.println(entrenador.getEntrenadorByTitulos(13, 10,0));    //ENTRENAODRYTITULOS     ----    FUNCIONA
        System.out.println(entrenador.allEntrenadores());           //VERLOSENTRENADORES    ----   FUNCIONA
        System.out.println(entrenador.lastID());                //LASTID    ---      FUNCIONA

        Entrenadores entrenador1 = new Entrenadores("Zinedine Zidane", 49, "Entrenador" , 16.000,22);   //CREAMOS OBJ ENTRENADOR
        entrenador1.setIdEntrenador(1);                 //SETEAMOS
        System.out.println(entrenador.updateEntrenadores(entrenador1));             //UPDATENTRENADOR    --   FUNCIONA


         */
 /*
        COMPROBAMOS JUGADORES
        
        //Jugadores jugador1 = new Jugadores("Thibaut Courtois", 28, "Portero", 0, 10, false, 1);
        
        //System.out.println(jugador.insertJugador("paco", 27, "delantero", 30, 0, false, 300));     //FUNCIONA 
        //System.out.println(jugador.deleteJugador(45));        //FUNCIONA
        //System.out.println(jugador.getJugador(5));    //FUNCIONA
        //System.out.println(jugador.getJugadorLike("Marco Asensio", 1, 0));        //FUNCIONA    
        //System.out.println(jugador.listaJugadores(0, 10));            //FUNCIONA
        //System.out.println(jugador.getJugadorByGoles(4, 25,0));           //FUNCIONA
        //System.out.println(jugador.allJugadores());           //FUNCIONA
        //System.out.println(jugador.lastID());             //FUNCIONA

        
        //Jugadores jugador1 = new Jugadores("Thibaut Courtois", 28, "Portero", 0, 10, false, 100);
        //jugador1.setIdJugador(1);                                     //FUNCIONA
        //System.out.println(jugador.updateJugador(jugador1));          //JUNTO
        
         */
    }
    /*
            public static void mostrarTest(String nombreTest, String query, int salidaEsperada, int salidaRecibida, boolean resultado){
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + nombreTest);
        System.out.println("Query que ejecuta: " + query);    
        System.out.println("Salida esperada:" + salidaEsperada);
        System.out.println("Salida recibida: " + salidaRecibida);
        if(resultado){
            System.out.println("Resultado: Correcto!");
        }else{
            System.out.println("Resultado: Error, la salida no es la esperada.");
        }
        
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
    }
     */
    /*
    
                System.out.println("Bienvenido a jugadores\n"
                        + "A continuacion te damos el listado de acciones para desarrollar en esta tabla\n"
                        + "Tan solo debes poner el valor de la derecha de cada proceso para hacer la consulta\n"
                        + "1º Obtener ultimo id   ----> last\n"
                        + "2º Obtener a un jugador buscandolo por su ID   ---->  get\n"
                        + "3º Insertar un nuevo jugador  ------>   insert\n"
                        + "4º Borrar un jugador   ------>   delete\n"
                        + "5º Ver todos los jugadores    ---->  all\n"
                        + "6º Listado con los jugadores   ---->  list\n"
                        + "7º Actualizar al jugador   ----> update\n"
                        + "8º Obtener jugador por nombre  ---->   name\n"
                        + "9º Obtener jugadir por goles   ------>  goal\n"
                        + "10º Finalizar las consultas  -------> finish");

                String jug = " ";
                String jugNombre = " ";
                String jugPosicion = " ";
                int juga;
                int jugaEdad;
                int jugaGoles;
                int jugaSalario;
                int jugaDorsal;
                boolean lesionado;
                Scanner scjug = new Scanner(System.in);
                Scanner scjuga = new Scanner(System.in);
                Scanner sclesion = new Scanner(System.in);
                jug = scjug.nextLine();

                System.out.println("");
                switch (jug) {

                    case "last":
                        System.out.println("*******************");
                        jugador.lastID();                                   //DONE
                        System.out.println("*******************");
                        break;

                    case "get":
                        System.out.println("Introduce el ID que deseas buscar");
                        juga = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.getJugador(juga);                          //DONE
                        System.out.println("*******************");
                        break;

                    case "insert":
                        System.out.println("Vamos a introducir al nuevo jugador, rellena los siguientes datos\n"
                                + "Nombre: ");
                        jugNombre = scjug.nextLine();
                        System.out.println("Edad: ");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("Posicion: ");
                        jugPosicion = scjug.nextLine();
                        System.out.println("Goles: ");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Salario: ");
                        jugaSalario = scjuga.nextInt();                     //DONE
                        System.out.println("Lesionado: ");
                        lesionado = sclesion.nextBoolean();
                        System.out.println("Dorsal: ");
                        jugaDorsal = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.insertJugador(jugNombre, jugaEdad, jugPosicion, jugaGoles, jugaSalario, cerramos, jugaDorsal);
                        System.out.println("*******************");
                        break;

                    case "delete":
                        System.out.println("Introduce el ID del jugador que desear eliminar");
                        juga = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.deleteJugador(juga);                        //DONE
                        System.out.println("*******************");
                        break;

                    case "all":
                        System.out.println("*******************");
                        System.out.println("Numero de jugadores totales");
                        jugador.allJugadores();                             //DONE
                        System.out.println("*******************");
                        break;

                    case "list":
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("*******************");                  //DONE
                        jugador.listaJugadores(juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "update":
                        System.out.println("Vamos a actualizar a un jugador, necesitamos su\n"
                                + "Nombre: ");
                        jugNombre = scjug.nextLine();
                        System.out.println("Edad: ");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("Posicion: ");
                        jugPosicion = scjug.nextLine();
                        System.out.println("Goles: ");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Salario: ");
                        jugaSalario = scjuga.nextInt();                     //DONE
                        System.out.println("Lesionado: ");
                        lesionado = sclesion.nextBoolean();
                        System.out.println("Dorsal: ");
                        jugaDorsal = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.updateJugador(jugador1);
                        System.out.println("*******************");
                        break;

                    case "name":
                        System.out.println("Introduce el nombre que queremos buscar");
                        jugNombre = scjug.nextLine();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();                            //DONE  
                        System.out.println("*******************");
                        jugador.getJugadorLike(jug, juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "goal":
                        System.out.println("Introduce el numero de goles que quieres buscar");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();                                                //DONE
                        System.out.println("*******************");
                        jugador.getJugadorByGoles(jugaGoles, juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "finish":
                        System.out.println("Finalizamos la consulta de esta tabla");
                        break;
                }
*/
}
