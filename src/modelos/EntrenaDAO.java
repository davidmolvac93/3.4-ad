/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface EntrenaDAO {
    
    public List<Entrena> listaEntrenados(int desde, int cuantos);

    public boolean insertEntrenos(String nombre);

    public boolean updateEntrenos(Entrena entrenamientos);

    public boolean deleteEntrenos(int idEntrenador_entrenadores, int idJugador_jugadores);

    public Entrena getEntrenos(int idEntrenador_entrenadores, int idJugador_jugadores);

    public List<Entrena> getEntrenaLike(String condicion);
}
