/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface JugadoresDAO {
    
    public List<Jugadores> listaJugadores(int desde, int cuantos);
    
    public boolean insertJugador(String nombre,int edad, String posicion, int goles, double salarioSemanal, boolean lesionado, int dorsal);

    public boolean updateJugador(Jugadores jugador);

    public boolean deleteJugador(int idJugador);

    public Jugadores getJugador(int idJugador);

    public List<Jugadores> getJugadorLike(String nombre, int desde, int cuantos);
    
    public List<Jugadores> getJugadorByGoles(int goles, int desde, int cuantos);
    
    public int allJugadores();
    
    public int lastID();
}
