/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Directiva {
    
    private int idDirectiva;
    private String nombre;
    private int edad;
    private double salario;
    private String puesto;

    public Directiva(String nombre, int edad, double salario, String puesto) {
        this.nombre = nombre;
        this.edad = edad;
        this.salario = salario;
        this.puesto = puesto;
        this.idDirectiva = idDirectiva;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getIdDirectiva() {
        return idDirectiva;
    }

    public void setIdDirectiva(int idDirectiva) {
        this.idDirectiva = idDirectiva;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "Directiva{" + "idDirectiva=" + idDirectiva + ", nombre=" + nombre + ", edad=" + edad + ", salario=" + salario + ", puesto=" + puesto + '}';
    }
    
    
}
