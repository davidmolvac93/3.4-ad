/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface RecuperaDAO {
    
    public List<Recupera> listaRecuperados(int desde, int cuantos);

    public boolean insertRecuperados(String nombre);

    public boolean updateRecuperados(Recupera recuperados);

    public boolean deleteRecuperados(int idJugador_jugadores, int idFisioterapeuta_fisioterapeutas);

    public Entrena getEntrenos(int idJugador_jugadores, int idFisioterapeuta_fisioterapeutas);

    public List<Recupera> getRecuperaLike(String condicion);    
}
