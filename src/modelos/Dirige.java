/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Dirige {
    
        
    private int idDirectiva_directiva;
    private int idEntrenador_entrenadores;

    public Dirige(int idDirectiva_directiva, int idEntrenador_entrenadores) {
        this.idDirectiva_directiva = idDirectiva_directiva;
        this.idEntrenador_entrenadores = idEntrenador_entrenadores;
    }

    public int getIdDirectiva_directiva() {
        return idDirectiva_directiva;
    }

    public void setIdDirectiva_directiva(int idDirectiva_directiva) {
        this.idDirectiva_directiva = idDirectiva_directiva;
    }

    public int getIdEntrenador_entrenadores() {
        return idEntrenador_entrenadores;
    }

    public void setIdEntrenador_entrenadores(int idEntrenador_entrenadores) {
        this.idEntrenador_entrenadores = idEntrenador_entrenadores;
    }

    @Override
    public String toString() {
        return "Dirige{" + "idDirectiva_directiva=" + idDirectiva_directiva + ", idEntrenador_entrenadores=" + idEntrenador_entrenadores + '}';
    }
    
    
}
