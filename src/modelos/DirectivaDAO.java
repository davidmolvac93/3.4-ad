/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface DirectivaDAO {

    public List<Directiva> listaDirectivos(int desde, int cuantos);

    public boolean insertDirectivos(String nombre, int edad, double salario, String puesto);

    public boolean updateDirectivos(Directiva directivos);

    public boolean deleteDirectivos(int idDirectiva);

    public Directiva getDirectivos(int idDirectiva);

    public List<Directiva> getDirectivoLike(String nombre, int desde, int cuantos);
    
    public List<Directiva> getDirectivaByEdad (int edad, int desde, int cuantos);
    
    public int allDirectivos();
    
    public int lastID();
    
}
